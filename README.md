#App (pt-br)
##Descrição
Crie um app IOS que mostre uma listagem das temporadas de uma série ou show da sua escolha e ao clicar. Siga o mockup abaixo para fazer algo parecido.

Mockups

https://ibb.co/mwa52G

https://ibb.co/bR8Q2G

Para isso você deverá utilizar a API do Trakt (você deve se registrar no site para ter acesso à API).https://trakt.docs.apiary.io/#reference/seasons


##Requisitos

Funcionar a partir do IOS:8.*

É permitido o uso de bibliotecas externas

Tratamento de cenários de erro (ausência de conexão, erros do servidor)

Feedbacks de carregamento

Efeito de parallax no cabeçalho

Tornar a toolbar opaca conforme o scroll da listagem de temporadas

Testes automatizados

Layout diferente para tablet ou orientação landscape

Cache de imagens e da API (Caso a pessoa acesse a segunda vez offline, será possivel a visualização)

##Submissão
O candidato deverá implementar a solução e enviar um pull request para este repositório com a solução.

O processo de Pull Request funciona da seguinte maneira:

1.Candidato fará um fork desse repositório (não irá clonar direto!)

2.Fará seu projeto nesse fork.

3.Commitará e subirá as alterações para o SEU fork.

4.Pela interface do Bitbucket, irá enviar um Pull Request.

5.Se possível deixar o fork público para facilitar a inspeção do código




##App (en)
##Description
Create an IOS app that shows a list of season from a serie.You should use the mockup above as base for your development 

Mockups

https://ibb.co/mwa52G

https://ibb.co/bR8Q2G

In order to create this app you will use the Trakt API (you must sign up in the website to have access to the API).

##Requirements

Your app must work since IOS 8.*

It’s allowed to use external libraries

Handle error scenarios (no connectivity, server errors)

Loading feedback

Parallax effect on header scroll

Transform the toolbar in a opaque color during scroll

Automated tests

Different layout for tablet or landscape orientation

Cache of images and API (If the person accesses the second time offline, it will be possible to view)


###Submission
The candidate must implement the solution and send a pull request to this repository with the solution.

The Pull Request process works as follows:

1.Candidate will fork this repository (will not clone directly!)

2.It will make your project on that fork.

3.Commit and upload changes to your fork.

4.Through the Bitbucket interface, you will send a Pull Request.

5.If possible to leave the public fork to facilitate inspection of the code

